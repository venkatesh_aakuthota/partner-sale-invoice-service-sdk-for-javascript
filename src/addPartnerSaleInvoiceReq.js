export default class AddPartnerSaleInvoiceReq{

    _number:string;

    _file:Blob;

    _partnerSaleRegistrationId:number;

    /**
     * @param {string} number
     * @param {Blob} file
     * @param {number|null} partnerSaleRegistrationId
     */
    constructor(
        number:string,
        file:Blob,
        partnerSaleRegistrationId:number = null
    ){

        if(!number){
            throw new TypeError('number required');
        }
        this._number = number;

        if(!file){
            throw new TypeError('file required');
        }
        this._file = file;

        this._partnerSaleRegistrationId = partnerSaleRegistrationId;

    }

    /**
     * @returns {string}
     */
    get number():string {
        return this._number;
    }

    /**
     * @returns {Blob}
     */
    get file():Blob {
        return this._file;
    }

    /**
     * @returns {number|null}
     */
    get partnerSaleRegistrationId():number {
        return this._partnerSaleRegistrationId;
    }
}
