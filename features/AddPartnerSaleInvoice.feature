Feature: Add Partner Sale Invoice
  Adds a partner sale invoice

  Background:
    Given an addPartnerSaleInvoiceReq consists of:
      | attribute                 | validation | type   |
      | number                    | required   | string |
      | fileContent               | required   | byte[] |
      | fileExtension             | required   | string |
      | partnerSaleRegistrationId | optional   | number |

  Scenario: Success
    Given I provide an accessToken identifying me as a partner rep
    And provide a valid addPartnerSaleInvoiceReq
    When I execute addPartnerSaleInvoice
    Then a partner sale invoice is added to the partner-sale-invoice-service with the provided attributes
